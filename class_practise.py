class cat:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def eat(self):
        print(self.name," is eating.")

    def drink(self):
        print(self.name," is drinking.")

CAT = cat("Biubiu",3)
print(CAT.name)
print(CAT.age)
CAT.eat()
CAT.drink()

cat_list = [cat("Bob",12),cat("Abi",5)]
for single_cat in cat_list:
    print(single_cat.name)
    print(single_cat.age)
    single_cat.eat()
    single_cat.drink()